package situm.android.openglrotationmatrix;

import android.content.Context;
import android.opengl.GLSurfaceView;

/**
 * Created by Seker on 2/23/2016.
 */

public class myGlSurfaceView extends GLSurfaceView {

    public myGlSurfaceView(Context context, myRenderer myRender) {
        super(context);
        // Create an OpenGL ES 3.0 context.
        setEGLContextClientVersion(3);

        super.setEGLConfigChooser(8 , 8, 8, 8, 16, 0);
        // Set the Renderer for drawing on the GLSurfaceView
        setRenderer(myRender);

        // Render the view only when there is a change in the drawing data
        setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
    }

}
