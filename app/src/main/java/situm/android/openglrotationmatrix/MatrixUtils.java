package situm.android.openglrotationmatrix;

public class MatrixUtils {

    private static final String TAG = MatrixUtils.class.getSimpleName() ;

    public static float[] matrix3x3To4x4(float[] matrix9){
        if(matrix9==null){
            return null;
        }

        float[] matrix16 = new float[16];
        matrix16[0]=matrix9[0];
        matrix16[1]=matrix9[1];
        matrix16[2]=matrix9[2];
        matrix16[3]=0;
        matrix16[4]=matrix9[3];
        matrix16[5]=matrix9[4];
        matrix16[6]=matrix9[5];
        matrix16[7]=0;
        matrix16[8]=matrix9[6];
        matrix16[9]=matrix9[7];
        matrix16[10]=matrix9[8];
        matrix16[11]=0;
        matrix16[12]=0;
        matrix16[13]=0;
        matrix16[14]=0;
        matrix16[15]=1;

        return matrix16;
    }


}
