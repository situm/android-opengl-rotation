package situm.android.openglrotationmatrix;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class SensorListener implements SensorEventListener {

    private static final String TAG = SensorListener.class.getSimpleName() ;
    private SensorManager sensorManager;
    private  Callback callback;

    private final float[] rotationMatrix = new float[9];
     private final float[]  rotationVector = new float[4];


    public SensorListener(Context context, Callback callback) {
        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        this.callback = callback;


    }
    public void startListening(){
        Sensor rotationVector = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
        if (rotationVector != null) {
            sensorManager.registerListener(this, rotationVector,
                62500, SensorManager.SENSOR_DELAY_UI);
        }
    }


    @Override
    public void onSensorChanged(SensorEvent event) {

        if (event.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR) {
            System.arraycopy(event.values, 0, rotationVector,
                0, rotationVector.length);
            SensorManager.getRotationMatrixFromVector(rotationMatrix,rotationVector);
            if(rotationMatrix!=null) {
                callback.onRotationMatrixReceived(rotationMatrix);
            }

        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

}
