package situm.android.openglrotationmatrix;

import es.situm.sdk.model.location.Location;

public interface Callback {
    public abstract void onRotationMatrixReceived(float[] rotationMatrix);
    public abstract void onLocationReceived(Location location);
}
