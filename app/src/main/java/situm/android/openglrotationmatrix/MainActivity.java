package situm.android.openglrotationmatrix;

import android.Manifest.permission;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ConfigurationInfo;
import android.graphics.Color;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.TextView;
import es.situm.sdk.model.location.Location;
import java.util.Arrays;


public class MainActivity extends Activity{
    private final String TAG = getClass().getSimpleName();
    private myGlSurfaceView myview;
    private myRenderer myRender;
    private TextView textView;
    private Callback callback;

    private SensorListener sensorListener;
    private PositioningHelper positioningHelper;

    float[] situmRotationMatrix=null;
    float[] androidRotationMatrix=null;
    private Location currentLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (VERSION.SDK_INT >= VERSION_CODES.M) {
            requestPermissions(new String[]{permission.ACCESS_FINE_LOCATION,permission.BLUETOOTH_SCAN},0);
        }


        if (detectOpenGLES30()) {
            //so we know it a opengl 3.0 and use our extended GLsurfaceview.

            callback = new Callback() {
                @Override
                public void onRotationMatrixReceived(float[] rotationMatrix) {
                    androidRotationMatrix = rotationMatrix;
                    updateRotationMatricesToRender();
                    updateTextView();
                }

                @Override
                public void onLocationReceived(Location location) {
                    currentLocation = location;
                    situmRotationMatrix = (location.hasRotationMatrix())? location.getRotationMatrix():null;
                    updateRotationMatricesToRender();
                    updateTextView();
                }
            };


            textView  = new TextView(this);
            textView.setText("Init");
            textView.setTextColor(Color.WHITE);

            myRender = new myRenderer(this);
            myview =new myGlSurfaceView(this, myRender);

            setContentView(myview);
            addContentView(textView, new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

            sensorListener = new SensorListener(this, callback);
            sensorListener.startListening();

            positioningHelper = new PositioningHelper(this, callback);
            positioningHelper.startPositioning();

        } else {
            // This is where you could create an OpenGL ES 2.0 and/or 1.x compatible
            // renderer if you wanted to support both ES 1 and ES 2.
            Log.e("openglcube", "OpenGL ES 3.0 not supported on device.  Exiting...");
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy: ");
        this.positioningHelper.stopPositioning();
        this.callback = null;
        super.onDestroy();
    }


    private boolean detectOpenGLES30() {
        ActivityManager am =
                (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        ConfigurationInfo info = am.getDeviceConfigurationInfo();
        return (info.reqGlEsVersion >= 0x30000);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    public void updateTextView() {
        String text = (currentLocation!=null) ?
            "device Id: "+currentLocation.getDeviceId()+" \n "+
            "building Id: "+    currentLocation.getBuildingIdentifier()+" \n"+
                "is indoor: "+currentLocation.isIndoor()+"\n"+
                "bearing: "+currentLocation.getBearing().radians()+"\n"+
                "pitch: "+currentLocation.getPitch().radians()+"\n"+
                "roll: "+currentLocation.getRoll().radians()
            : "No Current Location";
        text+= (situmRotationMatrix!=null) ? "\n Situm Rotation Matrix": "\n Android Rotation Matrix";
        text+="\n Android Rotation Matrix";
        textView.setText(text);
        textView.setTextColor(Color.WHITE);
    }

    private void updateRotationMatricesToRender() {
        float[] pyramid1RotationMatrix = (situmRotationMatrix!=null) ? situmRotationMatrix: androidRotationMatrix;
        float[] pyramid2RotationMatrix = androidRotationMatrix;
        this.myRender.setPyramid1RotationMatrix(pyramid1RotationMatrix);
        this.myRender.setPyramid2RotationMatrix(pyramid2RotationMatrix);
    }
}
