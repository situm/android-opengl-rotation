package situm.android.openglrotationmatrix;

import android.content.Context;
import android.util.Log;
import es.situm.sdk.SitumSdk;
import es.situm.sdk.error.Error;
import es.situm.sdk.location.LocationListener;
import es.situm.sdk.location.LocationManager;
import es.situm.sdk.location.LocationRequest;
import es.situm.sdk.location.LocationStatus;
import es.situm.sdk.location.OutdoorLocationOptions;
import es.situm.sdk.location.OutdoorLocationOptions.BuildingDetector;
import es.situm.sdk.model.location.Location;

public class PositioningHelper {
    private static final String TAG = PositioningHelper.class.getSimpleName();
    private Callback callback;
    private LocationManager locationManager;

    public PositioningHelper(Context context, Callback callback) {
        try {
            SitumSdk.init(context);
            this.locationManager = SitumSdk.locationManager();
            this.callback = callback;
            Log.i(TAG, "Positioning helper init");
        }
        catch (Exception e) {
            Log.e("tag","exception sdk situmsdkinit: "+e.toString());
        }
    }


    private LocationListener locationListener = new LocationListener() {

        @Override
        public void onLocationChanged(Location location) {
            callback.onLocationReceived(location);
        }

        @Override
        public void onStatusChanged(LocationStatus status) {
            Log.i(TAG, "onStatusChanged() called with: status = [" + status + "]");
        }

        @Override
        public void onError(Error error) {
            Log.e(TAG, "onError() called with: error = [" + error + "]");
        }


    };

    public void startPositioning() {

        OutdoorLocationOptions outdoorLocationOptions = new OutdoorLocationOptions.Builder()
            .buildingDetector(BuildingDetector.WIFI_AND_BLE)
            .build();

        LocationRequest locationRequest = new LocationRequest.Builder()
            .useDeadReckoning(true)
            .useWifi(true)
            .useGps(true)
            .outdoorLocationOptions(outdoorLocationOptions)
            .build();

        this.locationManager.requestLocationUpdates(locationRequest, locationListener);

    }

    public void stopPositioning() {
        SitumSdk.locationManager().removeUpdates(locationListener);
    }

}
