
# Situm SDK rotation matrix  #

Example application that shows how the situm rotation matrix works.  
The application starts the positioning and displays two pyramids on the screen. 
The upper one shows the orientation estimated by Situm when positioning indoors and "dead reckoning" mode is active, and the android one when positioning outdoors.
The lower pyramid always shows the android pyramid as a reference.

 
## How to run this app

In order to get this example working you must follow this steps:

1. Create a Situm account, an Api key and a building. Just follow the steps in [this link](https://situm.com/docs/01-introduction/#3-toc-title)

2. Set your credentials in the app. Go to the `AndroidManifest.xml` file and edit this two fields


``` xml
<meta-data
    android:name="es.situm.sdk.API_USER"
    android:value="API_USER_EMAIL" />
<meta-data
    android:name="es.situm.sdk.API_KEY"
    android:value="API_KEY" />
```

---

## Submitting contributions

You will need to sign a Contributor License Agreement (CLA) before making a submission. [Learn more here](https://situm.com/contributions/). 

---
## License
This project is licensed under the MIT - see the [LICENSE](./LICENSE) file for further details.

---

## More information

More info is available at our [Developers Page](https://situm.com/docs/01-introduction/).

---

## Support information

For any question or bug report, please send an email to [support@situm.es](mailto:support@situm.es)